#for new users

System requirements

  - pip3
  - python 3
  - django version 2.2

Now,create python virtual environment for project:
    
    python3 -m venv env

To activate it using command : 

    source env/bin/activate

To install required packages:  

    pip3 install -r requirements.txt 

Now you can run migrations inside the terminal:

    python3 manage.py migrate

And run the server inside backend container:
    
    python3 manage.py runserver  # Run this command to only start the web server

Open url http://127.0.0.1:8000/admin in browser for admin login
 
   Enter username: priyanshi
   password: priyanshi@1998 

For API view lookup for -
   
   http://127.0.0.1:8000/user-activity