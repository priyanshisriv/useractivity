import json

from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.exceptions import ValidationError
from rest_framework import serializers

class ForeignKeyField(serializers.RelatedField):
    """ cutom ForeignKey fields """

    def __init__(self, *args, **kwargs):

        self.__display_name = kwargs.pop('display_name', None)
        super(ForeignKeyField, self).__init__(*args, **kwargs)

    def get_queryset(self):
        """ if query set is function then call this function """
        if callable(self.queryset):
            return self.queryset(self.context['request'])
        else:
            return self.queryset

    def to_internal_value(self, value):
            try:
                return self.get_queryset().get(id=value)
            except ObjectDoesNotExist:
                raise ValidationError({'error': "Invalid id"})
    def to_representation(self, value):
        if self.__display_name:
          return getattr(value, self.__display_name)
        else:
            return value.id
