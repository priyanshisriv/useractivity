from record.models import Member,ActivityPeriod
from django.shortcuts import render
from .serializers import MemberSerializer,ActivitySerializer
from rest_framework import (
    viewsets, mixins, permissions, response, views, generics, filters, exceptions)
# Create your views here.

class MemberViewSet(viewsets.ModelViewSet):
    """ list branches, create new branch """
    model = Member
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Member.objects.all()
    serializer_class = MemberSerializer

class ActivityViewSet(viewsets.ModelViewSet):
    """ list branches, create new branch """
    model = ActivityPeriod
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ActivityPeriod.objects.all()
    serializer_class = ActivitySerializer
