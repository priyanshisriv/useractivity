from rest_framework.routers import DefaultRouter

from . import views

app_name = 'record'
router = DefaultRouter()
router.register("user-activity",views.MemberViewSet)
router.register("activity-period",views.ActivityViewSet)

urlpatterns = router.urls