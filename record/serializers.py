from .models import Member,ActivityPeriod
from rest_framework import serializers
from myproject.libs.fields import ForeignKeyField


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model =  ActivityPeriod
        fields = ['start_time','end_time']


class MemberSerializer(serializers.ModelSerializer):
    activity_periods= ActivitySerializer(many=True,read_only=True)
    class Meta:
        model =  Member
        fields = ['member_id', 'real_name', 'tz', 'activity_periods',]

