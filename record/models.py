from django.db import models
import pytz
from datetime import datetime
from pytz import timezone
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Member(models.Model):
    member_id = models.CharField(primary_key=True,max_length=10)
    real_name = models.CharField(max_length=50)
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    tz = models.CharField(max_length=32, choices=TIMEZONES, 
    default='UTC')
    activity_periods = models.ManyToManyField('ActivityPeriod')

    class Meta:
        verbose_name = _('Member')
        verbose_name_plural = _('Members')
  
    def __str__(self):
        return self.real_name 


class ActivityPeriod(models.Model):
    start_time = models.DateTimeField(default=datetime.now)
    end_time = models.DateTimeField(default=datetime.now)

    class Meta:
        verbose_name = _('ActivityPeriod')
        verbose_name_plural = _('ActivityPeriods')
  
